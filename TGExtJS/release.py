# Release information about TGExtJS

version = "0.2.3b"

description = "ExtJS packaged as collection of TurboGears widgets."
# long_description = "More description about your plan"
author = "W-Mark Kubacki"
email = "wmark.tgextjs@hurrikane.de"
copyright = "Author; ExtJS parts: Copyright (c) 2006-2007, Ext JS, LLC"

# if it's open source, you might want to specify these
url = "http://tgwidgets.ossdl.de/"
download_url = "http://static.ossdl.de/tgwidgets/downloads/"
license = "LGPL"
